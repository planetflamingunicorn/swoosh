package com.jojajones.swoosh

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_skill_select.*

class SkillSelect : BaseActivity() {
    lateinit var player:Player
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skill_select)

        player = intent.getParcelableExtra(PLAYER)
        finishBtn.setOnClickListener {
            if(ballerBtn.isChecked || beginnerBtn.isChecked) {
                val finishIntent = Intent(this, FinalActivity::class.java)
                finishIntent.putExtra(PLAYER, player)
                startActivity(finishIntent)
            } else {
                Toast.makeText(this, "Please select a skill first", Toast.LENGTH_SHORT).show()

            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(PLAYER, player)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if(savedInstanceState != null){
            player = savedInstanceState.getParcelable(PLAYER)
        }

    }

    fun onBeginnerClicked(view: View){
        ballerBtn.isChecked = false
        player.skill = "beginner"
    }

    fun onBallerClicked(view: View){
        beginnerBtn.isChecked = false
        player.skill = "baller"
    }
}
