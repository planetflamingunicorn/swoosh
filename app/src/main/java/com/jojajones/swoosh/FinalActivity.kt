package com.jojajones.swoosh

import android.annotation.SuppressLint
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_final.*

class FinalActivity : BaseActivity() {
    lateinit var player : Player
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final)
        player = intent.getParcelableExtra(PLAYER)

        finalText.text = "Looking for a ${player.league} ${player.skill} league near you..."
    }
}
