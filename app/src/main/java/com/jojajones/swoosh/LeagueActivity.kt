package com.jojajones.swoosh

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : BaseActivity() {
    var player = Player("","")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(PLAYER, player)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if(savedInstanceState != null){
            player = savedInstanceState.getParcelable(PLAYER)
        }

    }

    fun leagueNextClicked(view: View){
        if (womensBtn.isChecked || mensBtn.isChecked || coedBtn.isChecked) {
            val skillIntent = Intent(this, SkillSelect::class.java)
            skillIntent.putExtra(PLAYER, player)
            startActivity(skillIntent)
        } else {
            Toast.makeText(this, "Please select a league first", Toast.LENGTH_SHORT).show()
        }
    }

    fun onMensClicked(view: View){
        womensBtn.isChecked = false
        coedBtn.isChecked = false
        player.league = "men's"
    }

    fun onWomensClicked(view: View){
        mensBtn.isChecked = false
        coedBtn.isChecked = false
        player.league = "women's"
    }

    fun onCoedClicked(view: View){
        mensBtn.isChecked = false
        womensBtn.isChecked = false
        player.league = "co-ed"
    }
}
